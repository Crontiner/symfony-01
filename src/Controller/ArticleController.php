<?php
namespace App\Controller;

use App\Entity\Article;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;


class ArticleController extends Controller {

    /*
    @Route("/", name="article_list")
    @Method({"GET"})
    */
    public function index() {
        //return new Response('Hello!');

        $articles = $this->getDoctrine()->getRepository(Article::class)->findAll();
        
        //$articles = ['Article 1', 'Article 2'];

        return $this->render( 'articles/index.html.twig', array('articles' => $articles, 'name' => 'John Doe') );
    }

    
    /*
    @Route("/article/new", name="new_article")
    @Method({"GET", "POST"})
    */
    public function new(Request $request) {
        $article = new Article();
        
        $form = $this->createFormBuilder($article)

                    ->add( 'title', 
                            TextType::class, 
                            array('attr' => array('class' => 'form-control')) )

                    ->add( 'body', 
                            TextareaType::class, 
                            array(  'required' => FALSE, 
                                    'attr' => array('class' => 'form-control')) )
            
                    ->add( 'save',
                            SubmitType::class,
                            array('label'=> 'Create', 
                                  'attr' => array('class' => 'btn btn-primary mt-3') ) )
            
                    ->getForm();
        
        $form->handleRequest($request);
        
        if ( $form->isSubmitted() && $form->isValid() ) {
            $article = $form->getData();
            
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($article);
            $entityManager->flush();
            
            return $this->redirectToRoute('article_list');
        }
        
        return $this->render('articles/new.html.twig', array( 'form' => $form->createView() ));
    }

    
    /*
    @Route("/article/delete/{id}", name="delete_article")
    @Method({"DELETE"})
    */
    public function delete(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();
        $post = $em->getRepository(Article::class)->find($id);

        if (!$post) {
            return $this->redirectToRoute('article_list');
        }
        
        $em->remove($post);
        $em->flush();

        return $this->redirectToRoute('article_list');
    }


    /*
    @Route("/article/edit/{id}", name="edit_article")
    @Method({"GET", "POST"})
    */
    public function edit(Request $request, $id) {
        $article = new Article();
        $article = $this->getDoctrine()->getRepository(Article::class)->find($id);

        $form = $this->createFormBuilder($article)

                    ->add( 'title', 
                            TextType::class, 
                            array('attr' => array('class' => 'form-control')) )

                    ->add( 'body', 
                            TextareaType::class, 
                            array(  'required' => FALSE, 
                                    'attr' => array('class' => 'form-control')) )
            
                    ->add( 'save',
                            SubmitType::class,
                            array('label'=> 'Update', 
                                  'attr' => array('class' => 'btn btn-primary mt-3') ) )
            
                    ->getForm();
        
        $form->handleRequest($request);
        
        if ( $form->isSubmitted() && $form->isValid() ) {
            
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->flush();
            
            return $this->redirectToRoute('article_list');
        }

        return $this->render('articles/edit.html.twig', array(
            'form' => $form->createView(),
            'article' => $article
        ));
    }
    
    
    /*
    @Route("/article/{id}", name="article_show")
    */
    public function show($id) {
        $article = $this->getDoctrine()->getRepository(Article::class)->find($id);
        
        return $this->render('articles/show.html.twig', array('article' => $article));
    }
    

    /*
    @Route("/article/save", name="article_save")
    */
    public function save() {
        $entityManager = $this->getDoctrine()->getManager();
        $article = new Article();

        $article_id = rand(1,999);
        
        $article->setTitle('Article '. $article_id);
        $article->setBody('This is the body for article '. $article_id);
        
        $entityManager->persist($article);
        $entityManager->flush();

        return new Response('Saved an article with the id of  '.$article->getId());
    }
    
}