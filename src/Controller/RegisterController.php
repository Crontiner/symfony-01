<?php
namespace App\Controller;

use App\Entity\User;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;


class RegisterController extends Controller {


    /*
    @Route("/registration", name="registration")
    @Method({"GET", "POST"})
    */
    public function new(Request $request, UserPasswordEncoderInterface $encoder) {
        $User = new User();
        
        $form = $this->createFormBuilder($User)

                    ->add( 'username', 
                            TextType::class, 
                            array(  'required' => TRUE, 
                                    'attr' => array('class' => 'form-control')) )

                    ->add( 'password', 
                            TextType::class, 
                            array(  'required' => TRUE, 
                                    'attr' => array('class' => 'form-control')) )

                    ->add( 'email', 
                            TextType::class, 
                            array(  'required' => TRUE, 
                                    'attr' => array('class' => 'form-control')) )
            
                    ->add( 'save',
                            SubmitType::class,
                            array('label'=> 'Submit', 
                                  'attr' => array('class' => 'btn btn-primary mt-3') ) )
            
                    ->getForm();
        
        $form->handleRequest($request);
        
        if ( $form->isSubmitted() && $form->isValid() ) {
            $rForm = $form->getData();

			$username = $form->get('username')->getData();
			$password = $form->get('password')->getData();
			$email = $form->get('email')->getData();

			$password = $encoder->encodePassword($User, $password);

			//$User->setPassword($encoded);
			//$rForm->setData(array('password' => $password));

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($rForm);
            $entityManager->flush();

            return $this->redirectToRoute('login');
        }
        
        return $this->render('security/registration.html.twig', array( 'form' => $form->createView() ));
    }
    
}